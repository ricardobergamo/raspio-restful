#!/usr/bin/env python

import sys
import logging

activate_this = '/home/pi/.virtualenvs/gpio/bin/activate_this.py'
with open(activate_this) as f:
    code = compile(f.read(), activate_this, 'exec')
    exec(code, dict(__file__=activate_this))

ENV_DIR = '/home/pi/.virtualenvs/gpio'
APP_DIR = '/home/pi/www/raspio-restful'

sys.path.append(ENV_DIR)

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, APP_DIR)

from manager import app as application
