from flask_restful import Resource, abort
import RPi.GPIO as GPIO
from config import PINS

GPIO.setmode(GPIO.BOARD)


def abort_if_pin_doesnt_exist(pin_id):
    if pin_id not in PINS:
        abort(404, message="Pin {} doesn't exist".format(pin_id))


class Gpio(Resource):
    @staticmethod
    def get():
        return PINS


class GpioPin(Resource):
    @staticmethod
    def get(pin_id):
        abort_if_pin_doesnt_exist(pin_id)
        return PINS[pin_id]


class GpioPinLow(Resource):
    @staticmethod
    def get(pin_id):
        abort_if_pin_doesnt_exist(pin_id)
        pin = PINS[pin_id]
        pin['state'] = GPIO.LOW
        gpio_pin = int(pin['pin'])
        gpio_state = pin['state']
        GPIO.setup(gpio_pin, GPIO.OUT)
        GPIO.output(gpio_pin, gpio_state)
        data = {
            'pin': gpio_pin,
            'state': gpio_state
        }
        return data


class GpioPinHigh(Resource):
    @staticmethod
    def get(pin_id):
        abort_if_pin_doesnt_exist(pin_id)
        pin = PINS[pin_id]
        pin['state'] = GPIO.HIGH
        gpio_pin = int(pin['pin'])
        gpio_state = pin['state']
        GPIO.setup(gpio_pin, GPIO.OUT)
        GPIO.output(gpio_pin, gpio_state)
        data = {
            'pin': gpio_pin,
            'state': gpio_state
        }
        return data


class GpioReset(Resource):
    @staticmethod
    def get():
        for pin in PINS:
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(int(pin), GPIO.LOW)
            GPIO.cleanup()
        data = {
            'state': 'GPIO cleanup'
            }
        return data
