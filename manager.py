#!/usr/bin/env python

from flask import Flask, redirect
from flask_restful import Api, Resource
from flask_script import Manager, commands
from config import API_INFO, DEBUG, HOST, PORT
from gpio import Gpio, GpioPin, GpioPinLow, GpioPinHigh, GpioReset

app = Flask(__name__)
api = Api(app, catch_all_404s=True)
manager = Manager(app)

app.config.from_pyfile('config.py', silent=True)


class Api(Resource):
    @staticmethod
    def get():
        return API_INFO

api.add_resource(Api, '/api/v1', endpoint='api')
api.add_resource(Gpio, '/api/v1/pin', endpoint='pin-list')
api.add_resource(GpioPin, '/api/v1/pin/<pin_id>', endpoint='pin')
api.add_resource(GpioPinLow, '/api/v1/pin/<pin_id>/off', endpoint='pin-off')
api.add_resource(GpioPinHigh, '/api/v1/pin/<pin_id>/on', endpoint='pin-on')
api.add_resource(GpioReset, '/api/v1/pin/reset', endpoint='pin-reset')


@app.route('/')
def gpioapi():
    return redirect('api/v1')


@manager.shell
def make_shell_context():
    return dict(app=app, api=api)

manager.add_command(
    "runserver",
    commands.Server(HOST, PORT, DEBUG)
)

if __name__ == '__main__':
    manager.run()
