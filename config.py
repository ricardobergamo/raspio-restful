#!/usr/bin/env python

import os

TITLE = 'RaspIO Restful'
API_NAME = 'RaspberryPI GPIO API'
VERSION = '0.1'
AUTHOR = 'Ricardo Bergamo'

DEBUG = debug = True
HOST = host = '0.0.0.0'
PORT = port = 8080
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = '\x87*\xfb\xfd!\xa5\x9bz\x14\xc4\xaf'

API_INFO = {
    'Api': {'Name': API_NAME,
            'Version': VERSION,
            'Author': AUTHOR},
    'Resources': {'Pin list': '<host>:<port>/api/v1/pin',
                  'Pin details': '<host>:<port>/api/v1/pin/<pin number>',
                  'Pin ON': '<host>:<port>/api/v1/pin/<pin number>/on',
                  'Pin OFF': '<host>:<port>/api/v1/pin/<pin number>/off',
                  'Pin reset': '<host>:<port>/api/v1/pin/reset'}
}

PINS = {
    "03": {
        "pin": "03",
        "name": "gpio02",
        "state": "GPIO.LOW"
    },
    "05": {
        "pin": "05",
        "name": "gpio03",
        "state": "GPIO.LOW"
    },
    "07": {
        "pin": "07",
        "name": "gpio04",
        "state": "GPIO.LOW"
    },
    "08": {
        "pin": "08",
        "name": "gpio14",
        "state": "GPIO.LOW"
    },
    "10": {
        "pin": "10",
        "name": "gpio15",
        "state": "GPIO.LOW"
    },
    "11": {
        "pin": "11",
        "name": "gpio17",
        "state": "GPIO.LOW"
    },
    "12": {
        "pin": "12",
        "name": "gpio18",
        "state": "GPIO.LOW"
    },
    "13": {
        "pin": "13",
        "name": "gpio27",
        "state": "GPIO.LOW"
    },
    "15": {
        "pin": "15",
        "name": "gpio22",
        "state": "GPIO.LOW"
    },
    "16": {
        "pin": "16",
        "name": "gpio23",
        "state": "GPIO.LOW"
    },
    "18": {
        "pin": "18",
        "name": "gpio24",
        "state": "GPIO.LOW"
    },
    "19": {
        "pin": "19",
        "name": "gpio10",
        "state": "GPIO.LOW"
    },
    "21": {
        "pin": "21",
        "name": "gpio09",
        "state": "GPIO.LOW"
    },
    "22": {
        "pin": "22",
        "name": "gpio25",
        "state": "GPIO.LOW"
    },
    "23": {
        "pin": "23",
        "name": "gpio11",
        "state": "GPIO.LOW"
    },
    "24": {
        "pin": "24",
        "name": "gpio08",
        "state": "GPIO.LOW"
    },
    "26": {
        "pin": "26",
        "name": "gpio07",
        "state": "GPIO.LOW"
    },
    "29": {
        "pin": "29",
        "name": "gpio05",
        "state": "GPIO.LOW"
    },
    "31": {
        "pin": "31",
        "name": "gpio06",
        "state": "GPIO.LOW"
    },
    "32": {
        "pin": "32",
        "name": "gpio12",
        "state": "GPIO.LOW"
    },
    "33": {
        "pin": "33",
        "name": "gpio13",
        "state": "GPIO.LOW"
    },
    "35": {
        "pin": "35",
        "name": "gpio19",
        "state": "GPIO.LOW"
    },
    "36": {
        "pin": "36",
        "name": "gpio16",
        "state": "GPIO.LOW"
    },
    "37": {
        "pin": "37",
        "name": "gpio26",
        "state": "GPIO.LOW"
    },
    "38": {
        "pin": "38",
        "name": "gpio20",
        "state": "GPIO.LOW"
    },
    "40": {
        "pin": "40",
        "name": "gpio21",
        "state": "GPIO.LOW"
    }
}
